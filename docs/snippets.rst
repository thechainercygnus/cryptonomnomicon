.. code-block:: sql
    sql_create_price_history_table = """ CREATE TABLE IF NOT EXISTS price_history (
                                        id integer PRIMARY KEY,
                                        symbol text NOT NULL,
                                        usd_val integer NOT NULL,
                                        FOREIGN KEY (symbol) REFERENCES tracked (symbol)
                                        ); """