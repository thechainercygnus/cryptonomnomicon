from modules import gecko
from os import getenv
import datetime
import logging

# Set up logging
log_module = 'classes'
logger = logging.getLogger(__name__)
if getenv('LOG_LEVEL') == 'DEBUG':
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

# File handler for logging
handler = logging.FileHandler((f"./logs/{str(datetime.date.today())}-{log_module}-nomnom.log"))
if getenv('LOG_LEVEL') == 'DEBUG':
    handler.setLevel(logging.DEBUG)
else:
    handler.setLevel(logging.INFO)

# Logging format definition
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# Add the handler to the logger
logger.addHandler(handler)

class CryptoToken:
    def __init__(self, t_id, symbol, name):
        self.id = t_id
        self.symbol = symbol
        self.name = name
        self.tracked = False