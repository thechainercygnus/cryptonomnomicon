# cryptonomnomicon
Crypto Data Tool for Fun?

Hey Bryce, do this stuff next:

- [ ] `/config/base.json` file build from initial `/coins/list` query on launch to seed database
- [ ] Update `cryptonomnomicon:known_tokens` table to reflect .env `DEFAULT_TOKENS` as `tracked = 1`
- [ ] Function to create databases per tracked token.
- [ ] **Eventual** REST API so it can be interacted with (i.e. by Dexter)

For known_tokens gecko query, to restrict API calls refresh every 6 hours. Store output to a .json file in /config.

For evaluating known_tokens first query coins/list from gecko and it will return a list of tokens that CoinGecko watches, then  get list of known tokens from DB, iterate through list of DB.token_symbols and for each one look in the Gecko.known_tokens. If not found add to DB.known_tokens, otherwise continue.