from classes.cryptotoken import CryptoToken
from modules import dbconn
from modules import gecko
from uuid import uuid4
from time import sleep
import datetime
import logging
import os

try:
    os.mkdir('logs')
    logs_created = True
except:
    print("Log folder exists")
    logs_created = False

try:
    os.mkdir('database')
    database_created = True
except:
    print("Database folder exists")
    database_created = False

# Set up logging
log_module = 'core'
logger = logging.getLogger(__name__)
if os.getenv('LOG_LEVEL') == 'DEBUG':
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

# File handler for logging
handler = logging.FileHandler((f"./logs/{str(datetime.date.today())}-{log_module}-nomnom.log"))
handler.setLevel(logging.DEBUG)

# Logging format definition
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# Add the handler to the logger
logger.addHandler(handler)

# Configure Variables
tracked_tokens=[] # DO NOT CHANGE
db_file = os.path.join(os.getenv('DATABASE_PATH'),'cryptonomnomicon.db')
default_tokens = os.getenv('DEFAULT_TOKENS').split(",")

instance_guid = str(uuid4())

logger.debug(" START Instance ID: " + instance_guid)

if logs_created:
    logger.info(" Log folder created")
if database_created:
    logger.info(" Database folder created")

# Test Gecko module loaded correctly
if gecko.test_connection() == 200:
    logger.info(" GECKO: API Endpoint accessible")
else:
    logger.info(" GECKO: API Error")
    exit(1)

# Validate that the database is accessible
logger.info(" SQLITE: Creating DB Schema")
dbconn.create_master_schema(db_file)
token_list = gecko.enum_known_tokens()
try:
    for token in token_list:
        logger.debug(" Enumerating: " + str(token))
        token_meta = CryptoToken(token)
        logger.debug("   " + str(token) + ": " + token_meta.name)
        dbconn.enum_known_tokens(db_file,token_meta)
except:
    logger.debug("    Token List: " + str(token_list))

while True:
    now = datetime.datetime.now()
    current_hour = int(now.strftime("%H"))
    if dbconn.check_last_ran(db_file,current_hour):
        logger.info("Populating list of known tokens from API")
        gecko.enum_known_tokens()
        dbconn.enum_known_tokens(db_file)
        dbconn.track_default(db_file)
    sleep(15)


logger.debug(" STOP Instance ID: " + instance_guid)