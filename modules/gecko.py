from time import sleep
import datetime
import logging
import requests
import json
import os
import pickle

# Set up logging
log_module = "gecko"
logger = logging.getLogger(__name__)
if os.getenv('LOG_LEVEL') == 'DEBUG':
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

# File handler for logging
handler = logging.FileHandler((f"./logs/{str(datetime.date.today())}-{log_module}-nomnom.log"))
handler.setLevel(logging.DEBUG)

# Logging format definition
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# Add the handler to the logger
logger.addHandler(handler)

base_api_uri = "https://api.coingecko.com/api/v3"
pickle_file = "./config/temp.pkl"

def test_connection():
    request = base_api_uri + "/ping"
    response = requests.get(request)
    return_code = response.status_code
    return return_code

def price_check(token):
    search_term = token.lower().replace(' ','-')
    request = (f"{base_api_uri}/simple/price?ids={search_term}&vs_currencies=usd")
    response = requests.get(request)
    results = json.loads(response.text)
    token_price = float(results[search_term]['usd'])
    value = (f"${token_price}")
    return value

def enum_symbol(symbol):
    symbol = symbol.lower()
    request = (f"{base_api_uri}/coins/list")
    try:
        #response = requests.get(request)
        results = requests.get(request).json()
        logger.debug("   " + str(results))
        for entry in results:
            if entry['symbol'] == symbol.lower():
                symbol_name = entry['name']
                symbol_id = entry['id']
                symbol_enum = [symbol_id,symbol_name]
                return symbol_enum
                sleep(0.75)
    except json.JSONDecodeError as e:
        logger.critical(f" JSON Decoder Error: {str(e)}")
        logger.debug(f" Error occurs with: {symbol}")
    except requests.ConnectionError as e:
        logger.critical(f" Connection Error: {str(e)}")

def enum_known_tokens():
    # Delete any archived versions of the temp file that would cause DB errors
    if os.path.exists(pickle_file):
        os.remove(pickle_file)
        logger.info(f"Removed Pickle file at '{pickle_file}'")
    else:
        logger.info(f"Pickle file '{pickle_file}' not present")
    
    request = base_api_uri + "/coins/list"
    response = requests.get(request).json()

    with open(pickle_file, "wb") as output:
        pickle.dump(response, output, pickle.HIGHEST_PROTOCOL)
    logger.info(f"Pickle file created '{pickle_file}'")
    logger.debug("Response returned")
    return response