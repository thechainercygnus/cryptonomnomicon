from os import getenv,remove,path
from sqlite3 import Error
import datetime
import logging
import sqlite3
import pickle

# Set up logging
log_module = "dbconn"
logger = logging.getLogger(__name__)
if getenv('LOG_LEVEL') == 'DEBUG':
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

# File handler for logging
handler = logging.FileHandler((f"./logs/{str(datetime.date.today())}-{log_module}-nomnom.log"))
handler.setLevel(logging.DEBUG)

# Logging format definition
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# Add the handler to the logger
logger.addHandler(handler)

pickle_file = './config/temp.pkl'


def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        #logger.info("Connection successful")
        return conn
    except Error as e:
        logger.critical(f"ERR-LOC-001: {str(e)}")
        exit(1)


def execute_sql_query(conn, query):
    """ create a table from the query statement
    :param conn: Connection object
    :param query: a SQL statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(query)
        conn.commit()
        logger.debug(f"Connection successful. Sending Query: {query}")
    except Error as e:
        logger.critical(f"SQLEXECERR: {str(e)}")
        logger.debug(f"SQL: {str(query)}")
        exit(1)


def select_all_tracked_tokens(conn):
        tracked_tokens = []
        cur = conn.cursor()
        cur.execute("SELECT symbol FROM tracked")
        rows = cur.fetchall()
        
        for row in rows:
            tracked_tokens.append(row)
        return tracked_tokens


def create_master_schema(db_file):
    logger.info("Database File: " + db_file)

    sql_create_known_tokens_table = """
        CREATE TABLE IF NOT EXISTS known_tokens (
        symbol text PRIMARY KEY UNIQUE,
        name text NOT NULL,
        id text NOT NULL,
        tracked integer,
        CHECK(tracked >= 0 OR tracked <= 1)
    ); """

    sql_create_application_state_table = """
        CREATE TABLE IF NOT EXISTS application_state (
            condition text PRIMARY KEY UNIQUE,
            state integer NOT NULL,
            value integer NULL,
            CHECK(state >= 0 AND state <= 1)
        );"""
    
    # Create the connection to the database
    conn = create_connection(db_file)

    # Create the tables we need
    if conn is not None:
        # Create Known Tokens table
        execute_sql_query(conn, sql_create_known_tokens_table)
        logger.info("Known Tokens table created")

        execute_sql_query(conn, sql_create_application_state_table)
        logger.info("Application State table created")

    else:
        logger.critical("Cannot create database connection.")

def set_initial_state(db_file):
    statement = ""

def track_token(db_file, token_symbol):
    already_tracked = check_if_tracked(db_file,token_symbol)
    if not already_tracked:
        sql_add_tracked_token = (
            f"""UPDATE known_tokens SET tracked = 1 
            WHERE symbol = '{token_symbol}';""")
        logging.debug(sql_add_tracked_token)
        conn = create_connection(db_file)

        if conn is not None:
            execute_sql_query(conn, sql_add_tracked_token)
            logger.info(
                f"Marking {token_symbol} as tracked in tokens database")
            conn.commit()
        else:
            logger.critical("Cannot create database connection.")
    else:
        logger.info(f"{token_symbol} is already tracked. Skipping.")


def list_tracked_tokens(db_file):
    tracked_tokens = []
    logger.info("Connecting to list tokens")
    conn = create_connection(db_file)
    with conn:
        logger.info("List tokens successful")
        query_results = select_all_tracked_tokens(conn)
        for row in query_results:
            tracked_tokens.append(row[0])
        return tracked_tokens


def check_if_tracked(db_file,token):
    logger.info("Connecting to validate " + token)
    conn = create_connection(db_file)
    with conn:
        cur = conn.cursor()
        query = (
            f"""SELECT * FROM known_tokens 
            WHERE symbol = '{token.lower()}' AND tracked = 1;""")
        logger.debug(f"Connection successful. Sending Query: {query}")
        cur.execute(query)
        rows = cur.fetchall()
        logger.debug(rows)
        for row in rows:
            if row[0].upper() == token.upper():
                return True
            else:
                return False

def check_if_dupe(db_file,token):
    logger.info(f"Validating no duplicate entry for {token}")
    conn = create_connection(db_file)
    with conn:
        result = False
        cur = conn.cursor()
        query = (f"SELECT * FROM known_tokens WHERE symbol = '{token.lower()}';")
        logger.debug(f"Connection successful. Sending Query: {query}")
        cur.execute(query)
        rows = cur.fetchall()
        logger.debug(f" Query Results: {rows}")
        if rows:
            result = True
            logger.info(f"Duplicate Detected {token}")

        return result


def enum_known_tokens(db_file):
    with open(pickle_file, 'rb') as input:
        known_token_list = pickle.load(input)
    if path.exists(pickle_file):
        remove(pickle_file)
    sql_statement = "INSERT INTO known_tokens (symbol,id,name,tracked) VALUES (%s,%s,%s,0);"
    val = []
    added_symbols = []
    for known_token in known_token_list:
        duplicate = check_if_dupe(db_file,known_token['symbol'].lower())
        if known_token['symbol'] in added_symbols:
            continue
        
        if duplicate:
            continue
        else:
            val.append(({known_token['symbol']},{known_token['id']},{known_token['name']}))
            added_symbols.append(known_token['symbol'])

    logger.info("Populating known tokens from CoinGecko into DB as untracked")

    conn = create_connection(db_file)
    try:
        with conn:
            cur = conn.cursor()
            cur.executemany(sql_statement,val)
            logger.debug(cur.fetchall())
            logger.info(f"Wrote {cur.rowcount} lines")
            conn.commit()
    except Error as e:
        logger.critical(f"SQLEXECERR: {str(e)}")
        logger.debug(f"SQL: {str(sql_statement)}")
        logger.debug(f"VAL: {str(val)}")

    """if conn is not None:
        for statement in sql_statements:
            statement = statement.replace("\n","")
            execute_sql_query(conn, statement)
            logger.debug("Adding " + statement)
    else:
        logger.critical("Cannot create database connection.")"""


def track_default(db_file):
    logger.debug("Populating default tokens")
    def_tokens = (getenv('DEFAULT_TOKENS')).split(",")
    for token in def_tokens:
        logger.info(f"Adding {token} from defaults")
        track_token(db_file,token.lower())

def check_last_ran(db_file,cur_hour):
    logger.debug(f"Checking last run hour against: {cur_hour}")
    conn = create_connection(db_file)
    with conn:
        cur = conn.cursor()
        query = ("""
            SELECT value FROM application_state
            WHERE condition = 'known_token_update_time';
            """)
        logger.debug(f"Connection successful. Sending Query: {query}")
        cur.execute(query)
        rows = cur.fetchall()
        logger.debug(f"Query Results: {rows}")
        if rows:
            logger.debug(f"Checking {cur_hour} against {rows[0][0]}")
            logger.debug(f"Current Hour input has type: {type(cur_hour)}")
            logger.debug(f"Current Application State Value type: {type(rows[0][0])}")
            cur_hour = int(cur_hour)
            run_hour = int(rows[0][0])
            if cur_hour != run_hour:
                sql = (f"""
                    UPDATE application_state SET value = {cur_hour}
                    WHERE condition = 'known_token_update_time';
                """)
                cur.execute(sql)
                conn.commit()
                logger.debug(f"Set 'known_token_update_time' to {cur_hour}")
                logger.warn(f"Known Token Update because {cur_hour} != {run_hour}")
                logger.info(f"Next Known Token Update at {cur_hour + 2}")
                run_approved = True
            else:
                logger.info(f"Next Known Token Update at {run_hour + 2} hours")
                run_approved = False
        else:
            create_condition = (f"""
            INSERT INTO application_state(condition,state,value)
            VALUES ('known_token_update_time',1,{cur_hour});
            """)
            cur.execute(create_condition)
            conn.commit()
            logger.debug(f"Created condition: {create_condition}")
            logger.warn(f"Known Token Initialization at {cur_hour} hours.")
            run_approved = True
            logger.info(f"Next Known Token Update at {cur_hour + 2} hours")
        return run_approved