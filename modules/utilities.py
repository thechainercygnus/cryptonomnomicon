import datetime
import hashlib
import logging
import os

# Set up logging
logger = logging.getLogger(__name__)
if os.getenv('LOG_LEVEL') == 'DEBUG':
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

# File handler for logging
handler = logging.FileHandler(('./logs/' + str(datetime.date.today()) + "-tracker-utilities.log"))
handler.setLevel(logging.DEBUG)

# Logging format definition
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# Add the handler to the logger
logger.addHandler(handler)

def hash_object(object):
    sha1 = hashlib.sha1(object).hashdigest()
    return sha1